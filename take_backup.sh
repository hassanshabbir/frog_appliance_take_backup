#!/bin/bash

BACKUP_WORKAREA="/tmp/taking_backup"
MYSQL_USER="root"
MYSQL_PW="MalaysiaPilot109"
TIMESTAMP=`date +%Y-%m-%d-%H-%M-%S`

#check if apache is running, if yes, shut it down
ps auxw | grep httpd | grep -v grep > /dev/null

if [ $? == 0 ]
then 
  {
    /etc/init.d/httpd stop > /dev/null 
  }
fi

#lets make a separate place to mess about in
if [ ! -d $BACKUP_WORKAREA ]
then 
  {
    mkdir -p $BACKUP_WORKAREA
  }
fi

MYSQL_DB_NAME=`mysql -B -N -u$MYSQL_USER -p$MYSQL_PW -e "show databases" | grep frogos`
SCHOOL_CODE=`echo $MYSQL_DB_NAME | sed s/_/\\n/g | grep -v "frog"`

mysqldump -u$MYSQL_USER -p$MYSQL_PW $MYSQL_DB_NAME > $BACKUP_WORKAREA/db.sql
cp -r /srv/sites/$SCHOOL_CODE/user $BACKUP_WORKAREA/user
tar -czvf /mnt/backupusb/$TIMESTAMP.tar.gz $BACKUP_WORKAREA/db.sql $BACKUP_WORKAREA/user

/etc/init.d/httpd start > /dev/null